const mongoose = require("mongoose");
const logger = require("../logger");
require("dotenv").config();

const dbConnection = (uri) => {
  const db = mongoose.createConnection(uri);

  db.on("error", function (error) {
    logger.error(`MongoDB :: connection ${this.name} ${JSON.stringify(error)}`);
    db.close().catch(() =>
      logger.error(`MongoDB :: failed to close connection ${this.name}`)
    );
  });

  db.on("connected", function () {
    // mongoose.set("debug", function (col, method, query, doc) {
    //   logger.info(
    //     `MongoDB :: ${this.conn.name} ${col}.${method}(${JSON.stringify(
    //       query
    //     )},${JSON.stringify(doc)})`
    //   );
    // });
    console.log("connected",uri)
    logger.info(`MongoDB :: connected ${this.name}`);
  });

  db.on("disconnected", function () {
    console.log("disconnected",uri)

    logger.info(`MongoDB :: disconnected ${this.name}`);
  });

  return db;
};

const admin_connection = dbConnection("mongodb+srv://admin:2C3xXJwvgGBb1AUK@cluster.gd6eq7f.mongodb.net/syncupp-admin?retryWrites=true&w=majority");
const crm_connection = dbConnection("mongodb+srv://admin:2C3xXJwvgGBb1AUK@cluster.gd6eq7f.mongodb.net/syncupp-crm?retryWrites=true&w=majority");

module.exports = {
  admin_connection,
  crm_connection,
};
